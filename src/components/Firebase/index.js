import FirebaseContext, { withFirebase } from './ContextFirebase'
import Firebase from './Firebase'

export default Firebase

export { FirebaseContext, withFirebase }
